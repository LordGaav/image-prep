# {{crate}}

Current version: **{{version}}**

{{readme}}

# License

{{license}}, see LICENSE file for details.
