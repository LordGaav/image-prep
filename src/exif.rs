use std::collections::HashMap;
use std::io;
use std::path::Path;

pub type ExifInfo = HashMap<String, String>;

pub fn extract_exif_info(path: &Path) -> Result<ExifInfo, String> {
    let file = std::fs::File::open(path).unwrap();
    let mut imgreader = io::BufReader::new(file);
    let exif = match exif::Reader::new().read_from_container(&mut imgreader) {
        Ok(e) => e,
        Err(e) => return Err(e.to_string()),
    };
    let mut tags = HashMap::new();
    for f in exif.fields() {
        if f.tag.description().is_none() {
            continue;
        }
        let t = f.tag.to_string();
        let v = match &f.value {
            exif::Value::Ascii(r) => {
                let mut q = "".to_owned();
                for s in r {
                    q += std::str::from_utf8(s).unwrap();
                }
                q
            }
            _ => f.value.display_as(f.tag).to_string(),
        };
        tags.insert(t, v);
    }
    Ok(tags)
}
