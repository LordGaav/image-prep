use std::ffi::OsStr;
use std::fs::{create_dir_all, File};
use std::path::{Path, PathBuf};

use rand::{thread_rng, Rng};
use serde::Serialize;

pub type Filename<'a> = (&'a OsStr, &'a OsStr);

fn random_name() -> String {
    thread_rng()
        .sample_iter(rand::distributions::Alphanumeric)
        .take(8)
        .map(char::from)
        .collect::<String>()
}

pub fn permanent_temp_dir(prefix: &str) -> Result<PathBuf, String> {
    let name = std::env::temp_dir().join(format!("{}-{}", prefix, random_name()));
    match create_dir_all(name.clone()) {
        Ok(()) => Ok(name),
        Err(e) => Err(format!("failed to create temp dir: {}", e)),
    }
}

pub fn split_filename(input_path: &Path) -> Result<Filename, String> {
    let filestem = match Path::new(input_path).file_stem() {
        Some(f) => f,
        None => {
            return Err(format!(
                "could not extract filename from path '{:?}'",
                input_path
            ))
        }
    };
    let extension = match Path::new(input_path).extension() {
        Some(e) => e,
        None => {
            return Err(format!(
                "could not extract extension from path '{:?}",
                input_path
            ))
        }
    };
    Ok((filestem, extension))
}

pub fn save_json_file<T>(output_path: &Path, obj: &T, pretty: bool) -> Result<(), String>
where
    T: ?Sized + Serialize,
{
    let file = match File::create(output_path) {
        Ok(f) => f,
        Err(e) => return Err(format!("cannot open JSON file {:?}: {}", output_path, e)),
    };
    if pretty {
        match serde_json::to_writer_pretty(file, obj) {
            Ok(_) => Ok(()),
            Err(e) => Err(format!("failed to write JSON file: {}", e)),
        }
    } else {
        match serde_json::to_writer(file, obj) {
            Ok(_) => Ok(()),
            Err(e) => Err(format!("failed to write JSON file: {}", e)),
        }
    }
}
