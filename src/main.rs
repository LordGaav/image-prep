//! Image preparation and EXIF export tool
//!
//! # Modes
//!
//! Currently there is only a one-shot mode called `single` that processes
//! all images passed as arguments to the tool in one batch.
//!
//! # Usage
//!
//! By default, all images passed as CLI arguments are resized to 500 and
//! 1000 pixels on the longest size, using Lanczos3 as a resize algorithm.
//! For each image, the EXIF information is dumped and stored in the output
//! JSON index file. Optionally, the original image can be copied as-is to
//! the output folder.
//!
//! The index file contains all resized image filenames, and
//! looks like this:
//!
//! ```json
//! [
//!   {
//!     "original": {
//!       "dimension": {
//!         "width": 6016,
//!         "height": 4016
//!       },
//!       "exif": {
//!         "DateTime": "2023:08:13 13:48:28",
//!         "LensModel": "E 18-135mm F3.5-5.6 OSS",
//!         "Model": "ILCE-6400",
//!         "Make": "SONY",
//!         // etc
//!       }
//!     },
//!     "resized": [
//!       {
//!         "filename": "DSC00461-500px.jpg",
//!         "size": 500,
//!         "dimension": {
//!           "width": 500,
//!           "height": 334
//!         }
//!       },
//!       // repeated for each image size requested
//!     ]
//!   }
//! ]
//! ```
//!
//! This index file is meant to be used by other tools, for instance to
//! generate an image gallery in a static website.

use std::env;
use std::path::PathBuf;

use clap::Args;
use clap::Parser;
use clap::Subcommand;

use crate::common::save_json_file;

mod common;
mod exif;
mod image;

#[derive(Parser, Debug)]
#[clap(
    version,
    about,
    long_about = include_str!("../README.md")
)]
struct Cli {
    #[clap(subcommand)]
    command: Commands,

    /// Whether to display additional information. Specify multiple times for more details.
    #[arg(short, long, global = true, action = clap::ArgAction::Count, env("VERBOSE"))]
    verbose: u8,
}

#[derive(Args, Debug, Clone)]
pub struct ResizeArgs {
    /// Path to output the resized images to. If not specified, a new path will created
    /// in the OS's temp folder.
    #[clap(long, env("OUTPUT_PATH"))]
    output_path: Option<PathBuf>,
    /// Sizes to resize the input image to. The given size will be the longest side
    /// of the resized image.
    #[arg(long, env("SIZES"), value_parser, num_args = 1.., value_delimiter = ',', default_value = "500,1000")]
    sizes: Vec<u32>,
    /// Copy the original image to the output path
    #[arg(long, env("COPY_ORIGINAL"), default_value = "false")]
    copy_original: bool,
    /// Copy EXIF information from the input image to all resized images.
    /// Currently not implemented.
    #[arg(long, env("COPY_EXIF"), default_value = "false")]
    copy_exif: bool,
    /// Pretty format output JSON index file.
    #[arg(long, env("PRETTY_JSON"), default_value = "false")]
    pretty_json: bool,
    /// Input image file
    input_files: Vec<PathBuf>,
}

#[derive(Subcommand, Debug)]
enum Commands {
    /// Collect stats once and output to stdout.
    Single {
        #[command[flatten]]
        resize_args: ResizeArgs,
    },
}

fn prepare_output_path(resize_args: &ResizeArgs) -> Result<PathBuf, String> {
    let output_path: PathBuf;
    if resize_args.output_path.is_some() {
        output_path = match resize_args.output_path.as_ref().unwrap().canonicalize() {
            Ok(p) => p,
            Err(e) => {
                return Err(format!(
                    "failed to canonicalize path of '{:?}': {}",
                    resize_args.output_path, e
                ));
            }
        };
        if !output_path.exists() {
            return Err(format!("output folder {:?} does not exist", output_path));
        }
        log::info!("using existing output folder: {:?}", output_path);
    } else {
        output_path = match common::permanent_temp_dir("image-prep") {
            Ok(p) => p,
            Err(e) => {
                return Err(format!("failed to create temporary output folder: {}", e));
            }
        };
        log::info!("created temporary output folder: {:?}", output_path);
    }
    Ok(output_path)
}

fn cmd_single(resize_args: &ResizeArgs) -> Result<(), String> {
    let output_path = prepare_output_path(resize_args)?;
    let mut images = Vec::new();
    for input_file in &resize_args.input_files {
        let original = match std::fs::canonicalize(input_file) {
            Ok(o) => o,
            Err(e) => {
                return Err(format!(
                    "failed to canonicalize path of '{:?}': {}",
                    input_file, e
                ));
            }
        };
        log::info!("Processing {:?}", original);
        let input_exif = exif::extract_exif_info(input_file.as_path()).unwrap();
        log::debug!("Extracted EXIF data");
        let image_info = match image::process_image_set(
            input_file.as_path(),
            input_exif,
            output_path.as_path(),
            &resize_args.sizes,
            resize_args.copy_original,
        ) {
            Ok(p) => p,
            Err(e) => {
                return Err(format!("failed to process image: {}", e));
            }
        };
        images.push(image_info)
    }
    let json_index_file = &output_path.join("index.json");
    log::debug!("Saving JSON index file to {:?}", json_index_file);
    save_json_file(json_index_file, &images, resize_args.pretty_json)?;
    Ok(())
}

fn main() {
    let cli = Cli::parse();

    if env::var_os("RUST_LOG").is_none() {
        if cli.verbose > 0 {
            env::set_var("RUST_LOG", "debug");
        } else {
            env::set_var("RUST_LOG", "info");
        }
    }
    pretty_env_logger::try_init_timed().expect("failed to init logger: ");

    let result = match &cli.command {
        Commands::Single { resize_args } => cmd_single(resize_args),
    };

    match result {
        Ok(_) => log::info!("Done"),
        Err(e) => {
            log::error!("Command failed: {}", e);
            std::process::exit(exitcode::TEMPFAIL);
        }
    };
}
