use image::io::Reader as ImageReader;
use image::{imageops::Lanczos3, DynamicImage};
use serde::{Deserialize, Serialize};
use std::path::{Path, PathBuf};

use crate::common::{split_filename, Filename};
use crate::exif::ExifInfo;

#[derive(Serialize, Deserialize)]
pub struct Dimension {
    pub width: u32,
    pub height: u32,
}

#[derive(Serialize, Deserialize)]
pub struct OriginalImageInfo {
    pub dimension: Dimension,
    pub exif: ExifInfo,
}

#[derive(Serialize, Deserialize)]
pub struct ResizedImageInfo {
    pub filename: PathBuf,
    pub size: u32,
    pub dimension: Dimension,
}

#[derive(Serialize, Deserialize)]
pub struct ImageInfo {
    pub original: OriginalImageInfo,
    pub resized: Vec<ResizedImageInfo>,
}

impl OriginalImageInfo {
    fn new(img: &DynamicImage, exif: ExifInfo) -> OriginalImageInfo {
        OriginalImageInfo {
            exif,
            dimension: Dimension {
                width: img.width(),
                height: img.height(),
            },
        }
    }
}

impl ResizedImageInfo {
    fn new(img: &DynamicImage, filename: PathBuf, size: u32) -> ResizedImageInfo {
        ResizedImageInfo {
            filename,
            size,
            dimension: Dimension {
                width: img.width(),
                height: img.height(),
            },
        }
    }
}

fn resize_images(
    input_image: &DynamicImage,
    input_filename: Filename,
    output_folder: &Path,
    sizes: &[u32],
) -> Result<Vec<ResizedImageInfo>, String> {
    let mut resized_images = Vec::new();
    let (filestem, extension) = input_filename;
    for size in sizes.iter() {
        let new_filename = PathBuf::from(format!(
            "{}-{}px.{}",
            filestem.to_string_lossy(),
            size,
            extension.to_string_lossy()
        ));
        log::debug!("Resizing image to {} pixels", size);
        let resized_image = input_image.resize(*size, *size, Lanczos3);
        let output_path = output_folder.join(new_filename.as_path());
        log::debug!("Saving image to {:?}", output_path);
        match resized_image.save(output_path.as_path()) {
            Ok(()) => {
                resized_images.push(ResizedImageInfo::new(&resized_image, new_filename, *size))
            }
            Err(e) => return Err(format!("failed to save image: {}", e)),
        }
    }
    Ok(resized_images)
}

fn copy_original_image(
    input_image: &DynamicImage,
    input_path: &Path,
    input_filename: Filename,
    output_folder: &Path,
) -> Result<ResizedImageInfo, String> {
    let (filestem, extension) = input_filename;
    let original_copy_filename = PathBuf::from(format!(
        "{}-original.{}",
        filestem.to_string_lossy(),
        extension.to_string_lossy()
    ));
    let output_filename = output_folder.join(original_copy_filename.as_path());
    log::debug!("Copying original image to {:?}", output_filename);
    match std::fs::copy(input_path, output_filename.as_path()) {
        Ok(_) => {}
        Err(e) => return Err(format!("failed to copy original image: {}", e)),
    };
    Ok(ResizedImageInfo::new(
        input_image,
        original_copy_filename,
        input_image.width().max(input_image.height()),
    ))
}

pub fn process_image_set(
    input_image: &Path,
    input_exif: ExifInfo,
    output_folder: &Path,
    sizes: &[u32],
    copy_original: bool,
) -> Result<ImageInfo, String> {
    let input_filename: Filename = split_filename(input_image)?;
    let file = match ImageReader::open(input_image) {
        Ok(i) => i,
        Err(e) => return Err(format!("failed to open image: {}", e)),
    };
    let img = match file.decode() {
        Ok(i) => i,
        Err(e) => return Err(format!("failed to decode image: {}", e)),
    };
    let mut resized_images = resize_images(&img, input_filename, output_folder, sizes)?;
    if copy_original {
        resized_images.push(copy_original_image(
            &img,
            input_image,
            input_filename,
            output_folder,
        )?);
    }
    Ok(ImageInfo {
        original: OriginalImageInfo::new(&img, input_exif),
        resized: resized_images,
    })
}
