variables:
  CARGO_HOME: $CI_PROJECT_DIR/.cargo
  RUST_VERSION: 1.71.1
  CARGO_MAKE_VERSION: 0.36.11
  CARGO_MAKE_SHA256SUM: 0de56c00063a3f18d23ef386e3d3c55dddb37208f69a8592b36d283d2bc07db0
  CARGO_TARPAULIN_VERSION: 0.26.1
  CARGO_TARPAULIN_SHA256SUM: d8f07d68b2739a545ffa6f45b093922cb23d0726d086b3d4c405aa374b154186
  CARGO_HOST_TARGET: x86_64-unknown-linux-gnu
  PACKAGE_REGISTRY_URL: ${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic
  HELM_REGISTRY_URL: ${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/helm/api/stable/charts
  GITLAB_IMAGE_NAME: "$CI_REGISTRY_IMAGE"

stages:
  - test-build
  - publish
  - release

workflow:
  rules:
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'
      when: never
    - if: '$CI_COMMIT_REF_NAME == "master"'
      when: never
    - when: always

.test_build_steps: &test_build_steps
  stage: test-build
  image: rust:${RUST_VERSION}
  before_script:
    - |
      mkdir -p ${CARGO_HOME} &&
      test -f "$HOME/.cargo/config" && cp $HOME/.cargo/config ${CARGO_HOME}/config
    - test -n "${EXTRA_DEBS}" && apt-get update -qq && apt-get install -qqy ${EXTRA_DEBS}
    - test -n "${EXTRA_CARGO}" && cargo install --target ${CARGO_HOST_TARGET} ${EXTRA_CARGO}
    - rustup target install ${CARGO_BUILD_TARGET}
    - |
      test -z "${CI_COMMIT_TAG}" &&
      curl --location --fail --silent --show-error --output tarpaulin.tar.gz https://github.com/xd009642/tarpaulin/releases/download/${CARGO_TARPAULIN_VERSION}/cargo-tarpaulin-x86_64-unknown-linux-musl.tar.gz &&
      echo "${CARGO_TARPAULIN_SHA256SUM} *tarpaulin.tar.gz" | sha256sum -c - &&
      tar zxvf tarpaulin.tar.gz -C /usr/local/bin/ &&
      rm tarpaulin.tar.gz
    - |
      test -n "${CI_COMMIT_TAG}" &&
      curl --location --fail --silent --show-error --output make.zip https://github.com/sagiegurari/cargo-make/releases/download/${CARGO_MAKE_VERSION}/cargo-make-v${CARGO_MAKE_VERSION}-x86_64-unknown-linux-musl.zip &&
      echo "${CARGO_MAKE_SHA256SUM} *make.zip" | sha256sum -c - &&
      unzip -j make.zip '*/cargo-make' -d /usr/local/bin/ &&
      rm make.zip
    - |
      rustc --version &&
      cargo --version
  script:
    - |
      test -z "${CI_COMMIT_TAG}" && exec cargo tarpaulin --out Xml
    - |
      test -n "${CI_COMMIT_TAG}" && exec cargo make --profile release gitlab-ci-flow
  after_script:
    - |
      test -n "${CI_COMMIT_TAG}" && curl --fail --silent --show-error --header "JOB-TOKEN: ${CI_JOB_TOKEN}" --upload-file "target/${CARGO_BUILD_TARGET}/release/${BINARY_NAME}" "${PACKAGE_REGISTRY_URL}/${CI_COMMIT_TAG}/${CARGO_BUILD_TARGET}/${BINARY_NAME}"
  coverage: '/^\d+.\d+% coverage/'
  artifacts:
    reports:
      coverage_report:
        coverage_format: cobertura
        path: cobertura.xml

test-build:x86_64-unknown-linux-gnu:
  <<: *test_build_steps
  variables:
    CARGO_BUILD_TARGET: x86_64-unknown-linux-gnu
    BINARY_NAME: image-prep

test-build:x86_64-unknown-linux-musl:
  <<: *test_build_steps
  variables:
    CARGO_BUILD_TARGET: x86_64-unknown-linux-musl
    BINARY_NAME: image-prep
    EXTRA_DEBS: musl-tools

publish:crates.io:
  stage: publish
  needs:
    - "test-build:x86_64-unknown-linux-gnu"
    - "test-build:x86_64-unknown-linux-musl"
  rules:
    - if: $CI_COMMIT_TAG && $CARGO_REGISTRY_TOKEN
  image: rust:${RUST_VERSION}
  script:
    - cargo publish

release:manifest:
  stage: release
  image: registry.gitlab.com/gitlab-org/release-cli:latest
  needs:
    - "publish:crates.io"
  rules:
    - if: "$CI_COMMIT_TAG"
  script:
    - echo "Releasing..."
  release:
    name: "Release ${CI_COMMIT_TAG}"
    description: "Automatic release."
    tag_name: "${CI_COMMIT_TAG}"
    assets:
      links:
        - name: image-prep_${CI_COMMIT_TAG}_x86_64-unknown-linux-gnu
          url: ${PACKAGE_REGISTRY_URL}/${CI_COMMIT_TAG}/x86_64-unknown-linux-gnu/image-prep
          link_type: package
        - name: image-prep_${CI_COMMIT_TAG}_x86_64-unknown-linux-musl
          url: ${PACKAGE_REGISTRY_URL}/${CI_COMMIT_TAG}/x86_64-unknown-linux-musl/image-prep
          link_type: package
        - name: crates.io
          url: https://crates.io/crates/image-prep/${CI_COMMIT_TAG}
          link_type: other
