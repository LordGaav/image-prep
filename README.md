# image-prep

Current version: **0.1.0**

Image preparation and EXIF export tool

## Modes

Currently there is only a one-shot mode called `single` that processes
all images passed as arguments to the tool in one batch.

## Usage

By default, all images passed as CLI arguments are resized to 500 and
1000 pixels on the longest size, using Lanczos3 as a resize algorithm.
For each image, the EXIF information is dumped and stored in the output
JSON index file. Optionally, the original image can be copied as-is to
the output folder.

The index file contains all resized image filenames, and
looks like this:

```json
[
  {
    "original": {
      "dimension": {
        "width": 6016,
        "height": 4016
      },
      "exif": {
        "DateTime": "2023:08:13 13:48:28",
        "LensModel": "E 18-135mm F3.5-5.6 OSS",
        "Model": "ILCE-6400",
        "Make": "SONY",
        // etc
      }
    },
    "resized": [
      {
        "filename": "DSC00461-500px.jpg",
        "size": 500,
        "dimension": {
          "width": 500,
          "height": 334
        }
      },
      // repeated for each image size requested
    ]
  }
]
```

This index file is meant to be used by other tools, for instance to
generate an image gallery in a static website.

# License

GPL-3.0-only, see LICENSE file for details.
